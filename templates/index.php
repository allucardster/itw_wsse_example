<h1>Hello <?php echo $_SESSION['user']->username ?></h1>
<hr />
<div>
    <a href="index?logout">Logout</a>
</div>
<div>
    <h2>Restaurants</h2>
    <?php
    if(!isset($_SESSION['restaurants']) || !count($_SESSION['restaurants'])) {
    ?>
    <em>No records found!</em>
    <?php
    } else {
    ?>
    <ul>
    <?php
    foreach ($_SESSION['restaurants'] as $restaurant) {
    ?>
        <li><?php echo $restaurant->name; ?></li>
    <?php
    }
    ?>
    </ul>
    <?php
    }
    ?>
</div>