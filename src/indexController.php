<?php
include_once('Util/ApiClient.php');
include_once('Util/Security.php');
$security = new Security();
$client = new ApiClient();
if(count($_POST) && isset($_POST['_username']) && isset($_POST['_password'])) {
    $result = $security->authenticateUser($_POST['_username'], $_POST['_password']);
    if(!isset($result->error)) {
        $restaurantsResult = $client->getAPIData('/api/personal/user/restaurants');
        if(isset($restaurantsResult->success) && $restaurantsResult->success && !isset($restaurantsResult->error) && !$restaurantsResult->error) {
            $_SESSION['restaurants'] = $restaurantsResult->response;
        }
    }
} else if (isset($_GET['logout'])) {
    $security->logout();
}

if(isset($_GET['xwsse']))
{
    if(isset($_SESSION['user']))
    {
        echo $client->getWSSEHeader($_SESSION['user']->username, $_SESSION['user']->password);
    }
}
?>