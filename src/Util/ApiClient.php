<?php
/**
 * ApiClient class
 */
class ApiClient
{
    const ITW_TIMEZONE = 'America/Los_Angeles';
    const ITW_URL = "http://itw.strsocial.com";

    /**
     * Send a POST request
     * 
     * @param  string $url
     * @param  array  $headers
     * @param  array  $params
     * @return \stdClass
     */
    public function post($url, $headers = array(), $params = array())
    {
        // Build the cURL session
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, count($params));
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POSTFIELDS,  http_build_query($params));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        // Send the request and check the response
        $object = new stdClass();
        if (($result = curl_exec($ch)) === FALSE) {
            $object->success = FALSE;
            $object->error = curl_error($ch);
            $object->response = null;
        } else {
            $object->success = TRUE;
            $object->error = null;
            $object->response = json_decode($result);
        }
        curl_close($ch);
        return $object;
    }

    /**
     * Send a GET request
     * 
     * @param  string $url
     * @param  array  $headers
     * @param  array  $params
     * @return \stdClass
     */
    public function get($url, $headers = array(), $params = array())
    {
        if(count($params)) {
            $url = $url . (strpos($url, '?') === FALSE ? '?' : '') . http_build_query($params);
        }
        // Build the cURL session
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPGET, TRUE);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        // Send the request and check the response
        $object = new stdClass();
        if (($result = curl_exec($ch)) === FALSE) {
            $object->success = FALSE;
            $object->error = curl_error($ch);
            $object->response = null;
        } else {
            $object->success = TRUE;
            $object->error = null;
            $object->response = json_decode($result);
        }
        curl_close($ch);
        return $object;
    }

    /**
    * Send a GET request to api $url
    * 
    * @param  string $url
    * @param  array  $params
    * @return null|\stdClass object
    */
    public function getAPIData($url, $params = array())
    {
        if(isset($_SESSION['user']))
        {
            $url = self::ITW_URL . $url;
            $user = $_SESSION['user'];
            $headers = array(
                'Authorization: Authorization profile="UsernameToken"',
                $this->getWSSEHeader($user->username, $user->password),
                'Cookie: PHPSESSID=' . session_id()
            );
            return $this->get($url, $headers, $params);
        }
        return null;
    }

    /**
     * Return "x-wsse" header by username and encripted password
     * 
     * @param  string $username
     * @param  string $encPassword
     * @return string
     */
    public function getWSSEHeader($username, $encPassword)
    {
        $now = new DateTime('now', new DateTimeZone(self::ITW_TIMEZONE));
        $created = $now->format('Y-m-d H:i:s');
        $nonce = md5(rand() . $created);
        $digest = $this->digestPassword(sprintf('%s%s%s', base64_decode($nonce), $created, $encPassword), '');
        return 'x-wsse : UsernameToken Username="' . $username . '", PasswordDigest="'. $digest .'",Nonce="'. $nonce .'", Created="'. $created .'"';
    }

    /**
    * Merges a password and a salt.
    *
    * @param string $password the password to be used
    * @param string $salt     the salt to be used
    *
    * @return string a merged password and salt
    */
    private function mergePasswordAndSalt($password, $salt)
    {
      if (empty($salt)) {
        return $password;
      }

      if (false !== strrpos($salt, '{') || false !== strrpos($salt, '}')) {
        throw new \InvalidArgumentException('Cannot use { or } in salt.');
      }

      return $password.'{'.$salt.'}';
    }

    /**
     * Get digest from raw password and salt
     * 
     * @param  string $raw
     * @param  string $salt
     * @return string
     */
    private function digestPassword($raw, $salt)
    {
      $salted = $this->mergePasswordAndSalt($raw, $salt);
      $digest = hash('sha512', $salted, true);
      for ($i = 1; $i < 5000; $i++) {
        $digest = hash('sha512', $digest.$salted, true);
      }
      return base64_encode($digest);
    }
}