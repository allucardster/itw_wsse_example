<?php

include_once('ApiClient.php');

/**
 * Security class
 */
class Security
{
    /**
    * Send a POST request to authentication service. If authentication
    * is successfully add user information to session. Otherwise show error
    * message
    * 
    * @param  string $username
    * @param  string $password
    * @return \stdClass object
    */
    public function authenticateUser($username, $password)
    {
        $url = ApiClient::ITW_URL . "/wsse/login_check";
        $postFields = array('_username' => $username, '_password' => $password);
        $headers = array(
            'Content-Type: application/x-www-form-urlencoded',
            'Cookie: PHPSESSID=' . session_id()
        );
        $apiClient = new ApiClient();
        $object = $apiClient->post($url, $headers, $postFields);
        $_SESSION['lastusername'] = $username;
        if(isset($object->error) && $object->error) {
            $_SESSION['error'] = $object->error;
        } else {
            $response = $object->response;
            if(isset($response->error) && $response->error)
            {
                $_SESSION['error'] = $response->error;
            }
            else
            {
                session_unset();
                $_SESSION['user'] = $response;
                $_SESSION['authenticated'] = true;
            }
        }
        return $object;
    }

    /**
    * Finish the session
    * 
    * @return void
    */
    public function logout()
    {
        session_unset();
        if (ini_get("session.use_cookies")) {
            $params = session_get_cookie_params();
            setcookie(session_name(), '', time() - 42000, $params["path"], $params["domain"], $params["secure"], $params["httponly"]);
        }
        session_destroy();
    }
}