<?php 
    session_start();
    $_SESSION['lastusername'] = '';
    $_SESSION['error'] = '';
    $_SESSION['authenticated'] = (isset($_SESSION['authenticated']) && $_SESSION['authenticated']);
    include('src/indexController.php');
?>
<!DOCTYPE html >
<html lang="en" xmlns="http://www.w3.org/1999/html">
    <head>
        <meta charset="utf-8" />
        <title>ITW - WSSE Example</title>
    </head>
    <body>
        <!-- Template -->
        <?php 
        if(!$_SESSION['authenticated']) { 
            $error = $_SESSION['error'];
            $lastusername = $_SESSION['lastusername'];
            include('templates/login.php');
        } else {
            include('templates/index.php');
        }
        ?>
    </body>
</html>